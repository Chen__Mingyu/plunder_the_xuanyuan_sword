// SetDlg.cpp : implementation file
//
#include "stdafx.h"
#include "Editor.h"
#include "SetDlg.h"

#include "scene.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSetDlg dialog
CSetDlg::CSetDlg(CWnd* pParent)
	: CDialog(CSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSetDlg)
	m_nID = 0;
	m_nType = -1;
	//}}AFX_DATA_INIT
}


void CSetDlg::Init(CScene* pData)
{
	m_pData = pData;
	m_nID   = 1;
	m_nType = 0;
}


void CSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSetDlg)
	DDX_Text(pDX, IDC_EDIT, m_nID);
	DDX_Radio(pDX, IDC_RADIO1, m_nType);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSetDlg, CDialog)
	//{{AFX_MSG_MAP(CSetDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSetDlg message handlers

void CSetDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData( TRUE );
	if( m_nID<1 || m_nID>32768 )
	{
		MessageBox("场ID值的取值范围是（1到32768）！",NULL,MB_OK);
		return;
	}
	m_pData->SetID(m_nID);
	m_pData->SetType(m_nType+1);
	CDialog::OnOK();
}

void CSetDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}
