// EditorDoc.cpp : implementation of the CEditorDoc class
//
#include "stdafx.h"
#include "Editor.h"
#include "SetDlg.h"
#include "EditorDoc.h"

#include "scene.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditorDoc

IMPLEMENT_DYNCREATE(CEditorDoc, CDocument)

BEGIN_MESSAGE_MAP(CEditorDoc, CDocument)
	//{{AFX_MSG_MAP(CEditorDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CEditorDoc, CDocument)
	//{{AFX_DISPATCH_MAP(CEditorDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//      DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IEditor to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {B2CA2FA9-23BE-4A94-9927-3919A891F952}
static const IID IID_IEditor =
{ 0xb2ca2fa9, 0x23be, 0x4a94, { 0x99, 0x27, 0x39, 0x19, 0xa8, 0x91, 0xf9, 0x52 } };

BEGIN_INTERFACE_MAP(CEditorDoc, CDocument)
	INTERFACE_PART(CEditorDoc, IID_IEditor, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditorDoc construction/destruction
class CScene;

CEditorDoc::CEditorDoc()
{
	// TODO: add one-time construction code here
	m_pData = new CScene;

	EnableAutomation();

	AfxOleLockApp();
}

CEditorDoc::~CEditorDoc()
{
	AfxOleUnlockApp();
}

/////////////////////////////////////////////////////////////////////////////
// CEditorDoc serialization

void CEditorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CEditorDoc diagnostics

#ifdef _DEBUG
void CEditorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CEditorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
BOOL CEditorDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	m_pData->New();
	//刷新视图
	//UpdateAllViews(NULL,0,NULL);
	//显示场类型设定对话框
	CSetDlg dlg;
	dlg.Init( m_pData );
	if( dlg.DoModal() == IDOK )
	{
		SetModifiedFlag( TRUE );
	}
	//
	return TRUE;
}

BOOL CEditorDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	// TODO: Add your specialized creation code here
	return ( m_pData->Open((char*)lpszPathName) );
}

BOOL CEditorDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	// TODO: Add your specialized code here and/or call the base class
	SetModifiedFlag(FALSE);//设置修改标志	
	m_pData->Save((char*)lpszPathName);
	return TRUE;
}
